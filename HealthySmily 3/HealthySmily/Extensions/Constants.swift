//
//  BaseUrl.swift
//  HealthySmily
//
//  Created by Zaid's MacBook on 30/01/2021.
//  Copyright © 2021 Zablesoft. All rights reserved.
//

import Foundation
import Foundation
let baseUrl = "http://192.168.43.90/zaid/"
let baseUrl2 = "http://192.168.43.90/zaid/api/"
//let imageBaseUrl = "https://srvc.biz/v1/doctor/photo/"


struct ConstantStrings {
    static let networkErrorTitle = "Network error!"
    static let networkErrorMessage = "Such as timeout, interuppted connection or un reachable host has occured"
    static let verificationError = "Please enter 4 digits code."
    static let inValidOrEmailPhoneNo = "Your email/password or password is incorrect."
    static let inValidPhoneNo = "Your number is incorrect."
    static let inValidVerificationCode = "Your verification code is incorrect."
    static let selectDoctor = "Please select speciality of doctor."
    static let noDoctorFound = "No doctor found."
    static let errorGettingMembers = "Error in getting members, please try again later"
    static let errorInDeletingMembers = "Something went wrong in deleting user, please try again later."
    static let errorInUpdatingMembers = "Something went wrong in updating ptofile, please try again later."
    static let errorInRegistering = "Something went wrong while registering, please try again later."
    static let errorInLoadingMsg = "Something went wrong while loading messages, please try again later."
    static let errorInSendMsg = "Something went wrong while sending messages, please try again later."
    static let errorInBookingAppointment = "Something went wrong in booking appointment, please try again later"
    
    static let AppColor = "0192D2"
}
