//
//  Extensions.swift
//  HealthySmily
//
//  Created by Zaid's MacBook on 30/01/2021.
//  Copyright © 2021 Zablesoft. All rights reserved.


import Foundation
import UIKit
import DropDown

extension UIViewController :UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    
    func dropDownSettings(){
        DropDown.appearance().textColor = UIColor.black
        DropDown.appearance().selectedTextColor = .hexStringToUIColor(hex: ConstantStrings.AppColor)
        //        DropDown.appearance().textFont = UIFont.systemFont(ofSize: 15)
        DropDown.appearance().backgroundColor = UIColor.white
        //        DropDown.appearance().selectionBackgroundColor = UIColor.lightGray
        //        DropDown.appearance().cellHeight = 60
    }
    
     func showToast(message : String, font: UIFont) {

         let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 135, y: self.view.frame.size.height-100, width: 275, height: 35))
         toastLabel.backgroundColor = UIColor(red: 1/255, green: 146/255, blue: 210/255, alpha: 1)
         toastLabel.textColor = UIColor.white
         toastLabel.font = font
         toastLabel.textAlignment = .center;
         toastLabel.text = message
         toastLabel.alpha = 1.0
         toastLabel.layer.cornerRadius = 10;
         toastLabel.clipsToBounds  =  true
         self.view.addSubview(toastLabel)
         UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
              toastLabel.alpha = 0.0
         }, completion: {(isCompleted) in
             toastLabel.removeFromSuperview()
         })
     }
//    func navigateToDashboard() {
//        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
//        vc.modalPresentationStyle = .fullScreen
//        self.present(vc, animated: true, completion: nil)
//    }
    func navigateToLoginVc(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
        vc.modalPresentationStyle = .fullScreen
        
        self.present(vc, animated: true, completion: nil)
    }
    
    func addlayoutToButtons(button :UIButton) {
        button.backgroundColor = .white
        button.layer.cornerRadius = 23
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor(red: 1/255, green: 146/255, blue: 210/255, alpha: 1).cgColor
        button.titleLabel?.textColor = UIColor(red: 1/255, green: 146/255, blue: 210/255, alpha: 1)
    }
    
    func addlayoutToTxtFields(textField:UITextField) {
        textField.layer.borderColor = UIColor(red: 1/255, green: 146/255, blue: 210/255, alpha: 1).cgColor
        textField.layer.cornerRadius = 25
        textField.layer.borderWidth = 1.0
        textField.textColor = UIColor(red: 1/255, green: 146/255, blue: 210/255, alpha: 1)
        textField.backgroundColor = .clear

    }
    func presentAlert(withTitle title: String, message : String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { action in
            print("You've pressed OK Button")
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    //MARK:- Split string
    public static func splitAtFirst(str: String, delimiter: String) -> (a: String, b: String)? {
       guard let upperIndex = (str.range(of: delimiter)?.upperBound), let lowerIndex = (str.range(of: delimiter)?.lowerBound) else { return nil }
       let firstPart: String = .init(str.prefix(upTo: lowerIndex))
       let lastPart: String = .init(str.suffix(from: upperIndex))
       return (firstPart, lastPart)
    }
    //MARK:- SHOW IMAGE PICKER

     func showImagePicker() {
         let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self

         let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a Source", preferredStyle: .actionSheet)

         actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action: UIAlertAction) in

         if UIImagePickerController.isSourceTypeAvailable(.camera) {
         imagePickerController.sourceType = .camera
         self.present(imagePickerController, animated: true, completion: nil)
         } else {
         print("Camera is not available.")
         }

         }))

         actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action: UIAlertAction) in
         imagePickerController.sourceType = .photoLibrary
         self.present(imagePickerController, animated: true, completion: nil)
         }))

         actionSheet.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))

         self.present(actionSheet, animated: true, completion: nil)
     }
    
}
extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    //Date Picker
    func addInputViewDatePicker(target: Any, selector: Selector) {

       let screenWidth = UIScreen.main.bounds.width

       //Add DatePicker as inputView
       let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))
       datePicker.datePickerMode = .date
       self.inputView = datePicker

       //Add Tool Bar as input AccessoryView
       let toolBar = UIToolbar()
        toolBar.sizeToFit()
       let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
       let doneBarButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector)
        let cancelBarButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPressed))

       toolBar.setItems([cancelBarButton, flexibleSpace, doneBarButton], animated: false)

       self.inputAccessoryView = toolBar
    }

      @objc func cancelPressed() {
        self.resignFirstResponder()
      }
}

//MARK:- UIView Extension
extension UIView {
    //Add layout to text fields
    func addLayout() {
        layer.cornerRadius = 23
        layer.borderWidth = 1
        layer.borderColor = UIColor(red: 1/255, green: 146/255, blue: 210/255, alpha: 1).cgColor
    }
    func dropDropDownShadow(scale: Bool = true) {
      layer.masksToBounds = false
        layer.shadowColor = UIColor.lightGray.cgColor
      layer.shadowOpacity = 0.5
      layer.shadowOffset = CGSize(width: -1, height: 1)
      layer.shadowRadius = 1
      //layer.cornerRadius = 20

    }
    func dropShadowToAllSides(scale: Bool = true) {
      layer.masksToBounds = false
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize(width: 0  , height: 0)
        layer.shadowRadius = 2
       layer.cornerRadius = 8

    }
    func addShadowToBottomLayer() {
        layer.shadowOffset = CGSize(width: 0, height: 1.75)
        layer.shadowRadius = 1
        layer.shadowOpacity = 0.6
        layer.shadowColor = UIColor(red: 1/255, green: 146/255, blue: 210/255, alpha: 1).cgColor

    }
    func dropLayoutToSearchbar() {
        layer.borderWidth = 1
        layer.cornerRadius = 8
        layer.borderColor = UIColor.lightGray.cgColor
    }
    //@available(iOS 13.0, *)
    func activityStartAnimating() {
        let backgroundView = UIView()
        backgroundView.frame = CGRect.init(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
        backgroundView.backgroundColor = UIColor.clear
        backgroundView.tag = 475647
        
        var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
        activityIndicator = UIActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 100, height: 100))
        activityIndicator.center = self.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .gray
        activityIndicator.color = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
        activityIndicator.startAnimating()
        self.isUserInteractionEnabled = false
        
        backgroundView.addSubview(activityIndicator)
        
        self.addSubview(backgroundView)
    }
    
    func activityStopAnimating() {
        if let background = viewWithTag(475647){
            background.removeFromSuperview()
        }
        self.isUserInteractionEnabled = true
    }
}

extension UIImageView {

   func makeRounded() {

       self.layer.borderWidth = 1
       self.layer.masksToBounds = false
       self.layer.borderColor = UIColor(red: 1/255, green: 146/255, blue: 210/255, alpha: 1).cgColor
       self.layer.cornerRadius = self.frame.height / 2
       self.clipsToBounds = true
}
}


//Round the 2 corners of view
extension CACornerMask {

    public static var leftBottom     : CACornerMask { get { return .layerMinXMaxYCorner}}
    public static var rightBottom    : CACornerMask { get { return .layerMaxXMaxYCorner}}
    public static var leftTop        : CACornerMask { get { return .layerMaxXMinYCorner}}
    public static var rightTop       : CACornerMask { get { return .layerMinXMinYCorner}}
}


extension CALayer {
    func applySketchShadow(color: UIColor = .black, alpha: Float = 0.5, x: CGFloat = 0, y: CGFloat = 2, blur: CGFloat = 4, spread: CGFloat = 0) {
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        shadowRadius = blur / 2.0
        if spread == 0 {
            shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
    }
    func roundCorners(_ mask:CACornerMask,corner:CGFloat) {
        self.maskedCorners = mask
        self.cornerRadius = corner
    }
}
//MARK:- UICOlor Extension
extension UIColor {
    class func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
//MARK:- String extension to validate email

extension String {
    var isValidEmail: Bool {
        NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}").evaluate(with: self)
    }
}
extension UIImage {

    /*
     @brief decode image base64
     */
    static func decodeBase64(toImage strEncodeData: String!) -> UIImage {

        if let decData = Data(base64Encoded: strEncodeData, options: .ignoreUnknownCharacters){
            return UIImage(data: decData) ?? UIImage(named: "upload2")!
        }
        return UIImage()
    }
}
extension UIView {
    @IBInspectable var borderColor: UIColor? {
        get {
            return layer.borderColor.map(UIColor.init)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
}
