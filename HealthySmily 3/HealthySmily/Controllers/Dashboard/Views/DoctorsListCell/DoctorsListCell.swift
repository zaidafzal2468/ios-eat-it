//
//  DoctorsListCell.swift
//  VirtualClinic
//
//  Created by Zaid's MacBook on 17/06/2020.
//  Copyright © 2020 Whetstonez. All rights reserved.
//

import UIKit
import Kingfisher

class DoctorsListCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var doctorProfileImageView: UIImageView!
    @IBOutlet weak var doctorAvailabilityStatus: UILabel!
    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var doctorFeesLabel: UILabel!
    @IBOutlet weak var doctorSpecialityFieldLabel: UILabel!
    @IBOutlet weak var patientsWaitingLAbel: UILabel!
    @IBOutlet weak var doctorVisitTimeLabel: UILabel!
    @IBOutlet weak var scheduleWithDoctorOutlet: UIButton!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        doctorProfileImageView.makeRounded()
        scheduleWithDoctorOutlet.layer.cornerRadius = 5
        containerView.layer.shadowOffset = CGSize(width: 0, height: 3)
        containerView.layer.shadowRadius = 1
        containerView.layer.shadowOpacity = 0.3
        containerView.layer.shadowColor = UIColor(red: 1/255, green: 146/255, blue: 210/255, alpha: 1).cgColor
        containerView.layer.cornerRadius = 20
        containerView.layer.borderWidth = 1.5
        containerView.layer.borderColor = UIColor(red: 1/255, green: 146/255, blue: 210/255, alpha: 1).cgColor
        



        // Initialization code
    }

    func configureCell(with item: DoctorsData?) {
        let url = URL(string: (item?.photo ?? ""))
        doctorProfileImageView.kf.setImage(with: url ,placeholder: UIImage(named: "doctorPix"))
        doctorAvailabilityStatus.text = "Available now"
        doctorNameLabel.text = "\(item?.name ?? "")"
        doctorSpecialityFieldLabel.text = item?.speciality
        let fee = item?.fees
        doctorFeesLabel.text = "Fee\(" ")\(fee ?? 200 )\(" RS")"
        doctorVisitTimeLabel.text = "20 min Visit time"
        patientsWaitingLAbel.text = "5 Patients Waiting"
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
