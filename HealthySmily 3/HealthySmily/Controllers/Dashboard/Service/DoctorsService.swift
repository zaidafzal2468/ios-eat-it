//
//  DoctorsService.swift
//  HealthySmily
//
//  Created by Zaid's MacBook on 31/01/2021.
//  Copyright © 2021 Zablesoft. All rights reserved.
//

import Foundation
import Alamofire
class DoctorService {
    /*
    //MARK:- Get Specialist Doctors List
    static func getSpecialistDoctor (complition :  @escaping ( (Result<GetSpecialityModel, Error>)-> ()) ) {
        NetworkManager.sharedInstance.sendGetRequest(url: baseUrl + "doctors", headers: nil, params: nil) { (response) in
    
            if response.error != nil {
                complition(.failure(response.error!))
            }else {
                do{
                    let json = try JSONSerialization.jsonObject(with: response.data!) as! Dictionary<String, AnyObject>
                    print("JSON ::\(json)")
                    let decoder = JSONDecoder()
                    let patient = try decoder.decode(GetSpecialityModel.self, from: response.data!)
                    complition(.success(patient))
                    print("Service Response \(patient)")
                } catch let jsonError{
                    print("Failure Response \(jsonError)")
                    complition(.failure(jsonError))
                }
            }
        }
    }
    //MARK:- Get Available dates and slotes
    static func getAvailableSlots(doctorId: Int,complition :  @escaping ( (Result<GetSpecialityModel, Error>)-> ()) ) {
        NetworkManager.sharedInstance.sendGetRequest(url: baseUrl + "doctors/\(doctorId)/availability", headers: nil, params: nil) { (response) in
    
            if response.error != nil {
                complition(.failure(response.error!))
            }else {
                do{
                    let json = JSON(response.data!)
                    print("JSON ::\(json)")
                    let decoder = JSONDecoder()
                    let patient = try decoder.decode(GetSpecialityModel.self, from: response.data!)
                    complition(.success(patient))
                    print("Service Response \(patient)")
                } catch let jsonError{
                    print("Failure Response \(jsonError)")
                    complition(.failure(jsonError))
                }
            }
        }
    }*/
    //MARK:- Search Doctor by name and id
    static func searchDoctors (nextPage: String , complition :  @escaping ( (Result<DoctorListModel, Error>)-> ())  ) {
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(Session.instance.getLoginToken!)"
        ]
        var url = "\(baseUrl2 + "doctors?")"

        if !nextPage.isEmpty {
            url.append("page=\(nextPage)")
        }

        let encodeUrl = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        print("Encode Url :\(encodeUrl)")

        NetworkManager.sharedInstance.sendGetRequest(url: encodeUrl, headers: headers, params: nil) { (response) in
            if response.response?.statusCode == 409 {
                print("4099999999")
               //  complition(.failure(error))
            }
//            if response.error != nil {
//                complition(.failure(response.error!))
//            }else {
                do{
                    let json = try JSONSerialization.jsonObject(with: response.data!) as! Dictionary<String, AnyObject>
                    print("JSON ::\(json)")
                    let decoder = JSONDecoder()
                    let patient = try decoder.decode(DoctorListModel.self, from: response.data!)
                    complition(.success(patient))
                    print("Service Response \(patient)")
                } catch let jsonError{
                    print("Get doctors Decode error \(jsonError)")
                    complition(.failure(jsonError))
                    print(response.debugDescription)
                }
       //     }
        }
    }
    //MARK:- Book Appointment with doctor

    static func bookAppointment (params:Parameters ,complition :  @escaping ( (_ success: Bool , _ error : AFError?)-> ()) ) {
        let token = Session.instance.getLoginToken!

        let headers: HTTPHeaders = [
            .accept("application/json"),
            .authorization("Bearer \(token)")
        ]
        NetworkManager.sharedInstance.sendPostRequest(url: baseUrl2 + "appointments/create", parameters: params, headers: headers) { (response) in
            if response.error != nil {
                complition(false, response.error)
            }else {
                complition(true, nil)
            }
        }
    }
}
