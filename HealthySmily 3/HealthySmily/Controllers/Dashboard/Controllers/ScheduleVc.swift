//
//  ScheduleVc.swift
//  HealthySmily
//
//  Created by Zaid's MacBook on 31/01/2021.
//  Copyright © 2021 Zablesoft. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import SwiftyJSON


class ScheduleVc: UIViewController {
    
    
    
    @IBOutlet weak var NextButtonOutlet: UIButton!
    @IBOutlet weak var selectDateOutlet: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var collectionViewAvailableSlot: UICollectionView!
    @IBOutlet weak var availableSlotsLAbel: UILabel!
    @IBOutlet weak var doctorNameLbl: UILabel!
    @IBOutlet weak var doctorSpecialityLbl: UILabel!
    @IBOutlet weak var noSlotAvailable: UILabel!
    @IBOutlet weak var doctorFeesLbl: UILabel!
    
    @IBOutlet weak var doctorImageView: UIImageView!
    
    let dropDown = DropDown()
    var selectedDateIndex = -1
    var indexPathOfCell : Int?
    var startTime = ""
    var dateTxt = ""
    var profileData: DoctorsData?
    var availaiableDateList: [Availabity] = []
    var startTimeList: [String] = []
    var startTimeArr = [String]()
    var scheduleArray = [Schedule]()




    override func viewDidLoad() {
        super.viewDidLoad()
        registerNIb()
        self.dropDownSettings()
        configureUI()
        
        showAlaiableDates()
       // self.navigationController?.navigationBar.isHidden = false

        // Do any additional setup after loading the view.
    }
    @IBAction func selectDateAction(_ sender: Any) {
        if availaiableDateList.count > 0 {
            dropDown.show()
        }
    }
    @IBAction func bookAppointmentPressed(_ sender: Any) {
        bookAppointment()
    }
    func registerNIb(){
        collectionViewAvailableSlot.register(UINib(nibName: "AvailableSlotsCell", bundle: nil), forCellWithReuseIdentifier: "AvailableSlotsCell")
    }
    func bookAppointment(){
        let params = [ "doctor_id": profileData?.id ?? 0 ,
                        "date_time": "\(dateTxt) \(startTime)"
                        //"date_time": "2020-12-01 13:02"

         ] as [String : Any]
        if Utils.isConnectedToNetwork() == true {
            self.view.activityStartAnimating()
        DoctorService.bookAppointment(params: params) { (success, error) in
            
            if success {
                DispatchQueue.main.async {
                    self.view.activityStopAnimating()
                    self.showToast(message: "Booked", font: UIFont.systemFont(ofSize: 12))
                    self.dismiss(animated: true, completion: nil)
                    print("successfully booked appointment")
                    // self.navigationController?.popViewController(animated: true)
                }
            }else {
                DispatchQueue.main.async {
                    self.view.activityStopAnimating()
                    print("Error in Appointment \(error?.localizedDescription ?? "")")
                    self.presentAlert(withTitle: "Error", message: ConstantStrings.errorInBookingAppointment)
                }
            }
        }
        }else{
            self.presentAlert(withTitle: ConstantStrings.networkErrorTitle, message: ConstantStrings.networkErrorMessage)
        }
    }
    func showAlaiableDates() {
        let headers: HTTPHeaders = [
            "Accept":"application/json",
            "Authorization": "Bearer \(Session.instance.getLoginToken!)"

          //  "Content-Type":"application/json"
        ]
        guard let id = profileData?.id else {
            return
        }
        NetworkManager.sharedInstance.sendGetRequest(url: baseUrl2 + "doctors/schedule/\(id)?from=2021-04-23&to=2021-04-26", headers: headers, params: nil) { (response) in
            print(response)
            if response.error != nil {
            }else {
                let list = Availabity.fromJSON(response.data!)
                self.availaiableDateList = list
                self.availableDatesInDropDown()
            }
        }
    }
    func configureUI() {
        containerView.addLayout()
       // NextButtonOutlet.addLayout()
        doctorImageView.makeRounded()
        // Update labels
        
        doctorNameLbl.text =  "\(profileData?.name ?? "")"
        doctorSpecialityLbl.text = profileData?.speciality

        let fee = profileData?.fees
        doctorFeesLbl.text = "Fee\(" ")\(fee ?? 200)\(" RS")"
        let url = URL(string: (profileData?.photo ?? ""))
        doctorImageView.kf.setImage(with: url ,placeholder: UIImage(named: "doctorPix"))

    }

    func availableDatesInDropDown() {
        dropDown.anchorView = selectDateOutlet
        var dataSetArray:[String] = []
        for item in availaiableDateList{
            dataSetArray.append(item.dateKey)
        }
        print("Without sort dates :\(dataSetArray)")
        let sortedDates = dataSetArray.sorted { $1 > $0 }
        print("Sorted Dates :\(sortedDates)")
        dropDown.dataSource = sortedDates
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.selectedDateIndex = index
            self.selectDateOutlet.isSelected = true
            self.selectDateOutlet.setTitle(sortedDates[index], for: .selected)
            self.dateTxt = sortedDates[index]
            self.availableSlots()
            self.collectionViewAvailableSlot.reloadData()
        }
    }
    func availableSlots() {
        if !availaiableDateList[selectedDateIndex].isSorted {
            availaiableDateList[selectedDateIndex].slotsList = availaiableDateList[selectedDateIndex].slotsList.sorted { (obj1, obj2) -> Bool in
                return obj2.timeKey > obj1.timeKey
            }
            availaiableDateList[selectedDateIndex].isSorted = true
        }
        print("Sorted Time :\(availaiableDateList[selectedDateIndex].slotsList)")
        collectionViewAvailableSlot.reloadData()
    }
}

//MARK:- CollectionView Delegates and DataSources
extension ScheduleVc : UICollectionViewDelegate ,UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selectedDateIndex != -1 {
            let count = availaiableDateList[selectedDateIndex].slotsList.count
            if count == 0 {
                noSlotAvailable.isHidden = false
            } else {
                noSlotAvailable.isHidden = true
            }
            return count
        }else {
            print("No slots avail")
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AvailableSlotsCell", for: indexPath) as! AvailableSlotsCell
        let obj = availaiableDateList[selectedDateIndex].slotsList[indexPath.row]
        cell.startTimeLabel.text = obj.startTime
        cell.endTimeLabel.text = obj.endTime
        cell.containerView.addLayout()
        cell.containerView.layer.cornerRadius = 12
        if (indexPath.row == indexPathOfCell)
        {
            cell.containerView.backgroundColor = .hexStringToUIColor(hex: ConstantStrings.AppColor)
            cell.startTimeLabel.textColor = .white
            cell.endTimeLabel.textColor = .white
            cell.ToLabel.textColor = .white
        }
        else
        {
            cell.containerView.backgroundColor = UIColor.white
            cell.startTimeLabel.textColor = .hexStringToUIColor(hex: ConstantStrings.AppColor)
            cell.endTimeLabel.textColor = .hexStringToUIColor(hex: ConstantStrings.AppColor)
            cell.ToLabel.textColor = .hexStringToUIColor(hex: ConstantStrings.AppColor)
            
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/4.45, height:collectionView.frame.size.height-20)
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        indexPathOfCell = indexPath.row
        startTime = availaiableDateList[selectedDateIndex].slotsList[indexPath.row].startTime
        collectionViewAvailableSlot.reloadData()
    }

}
