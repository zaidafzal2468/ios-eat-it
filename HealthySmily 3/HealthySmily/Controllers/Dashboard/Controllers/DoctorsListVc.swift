//
//  DoctorsListVc.swift
//  HealthySmily
//
//  Created by Zaid's MacBook on 31/01/2021.
//  Copyright © 2021 Zablesoft. All rights reserved.
//

import UIKit

class DoctorsListVc: UIViewController {
    
    
    @IBOutlet weak var doctorListTableView: UITableView!
    @IBOutlet weak var sideMenu: UIBarButtonItem!

    
    var doctorsArray = [DoctorsData]()
    var listOfDoctor: DoctorListModel?
    var index: DoctorsData?
    var fetchingMore = false
    var doctorName = ""
    var doctorID: Int?
    
    //left bar button Item
    let leftMenuButton = UIBarButtonItem(image: UIImage(named: "sideMenuIcon")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(sideDrawerAction))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenu.target = self.revealViewController()
        sideMenu.action = #selector(SWRevealViewController.revealToggle(_:))
        self.revealViewController().rearViewRevealWidth = 290
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())


        registerNib()
        getAllDoctors()
        //sideMenus()

        // Do any additional setup after loading the view.
    }
    @objc func sideDrawerAction(){
         print("clicked")
    }
    
    @objc func scheduleWithDoctorVisitAction(sender:UIButton!){
        print("Selected\(sender.tag)")
        let vc = storyboard?.instantiateViewController(withIdentifier: "ScheduleVc") as! ScheduleVc
        vc.profileData = doctorsArray[sender.tag]
        vc.modalPresentationStyle = .fullScreen
        //vc.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(vc,
                                                 animated: true)
        
    }
    func registerNib() {
        doctorListTableView.register(UINib(nibName: "DoctorsListCell", bundle: nil), forCellReuseIdentifier: "DoctorsListCell")
        doctorListTableView.register(UINib(nibName: "LoadingCell", bundle: nil), forCellReuseIdentifier: "loadingCell")
    }

    func getNextPage(){
        if listOfDoctor?.paginator?.has_more_pages == true {
            let nextPage = (self.listOfDoctor?.paginator?.current_page ?? 0) + 1
            if fetchingMore == false {
                fetchingMore = true
                self.doctorListTableView.reloadSections(IndexSet(integer: 1), with: .fade)
                print("Call New Api: \(nextPage)")
                getNextBlogs(pageNumber: nextPage)
            }
        }
    }
    func getAllDoctors(){
        DoctorService.searchDoctors(nextPage: "1") { (response) in
            self.fetchingMore = false
            switch response {
            case .success(let doctor):
                print("Response : \(doctor)")
                DispatchQueue.main.async {
                    self.view.activityStopAnimating()
                    self.listOfDoctor = doctor
                    self.doctorsArray.append(contentsOf : doctor.data ?? [DoctorsData]())
                    self.doctorListTableView.reloadData()
                }
                break
                
            case .failure(let error):
                DispatchQueue.main.async {
                    self.view.activityStopAnimating()
                    print("Error Getting doctor \(error.localizedDescription)")
                    self.presentAlert(withTitle: "Error", message: ConstantStrings.noDoctorFound)
                }
                break
            }
            
        }
    }
    func getNextBlogs(pageNumber:Int) {
        
       // let params = [ "page":pageNumber] as [String : Any]
        
        DoctorService.searchDoctors(nextPage: "\(pageNumber)") { (response) in
            self.fetchingMore = false
            switch response {
            case .success(let doctor):
                print("Response : \(doctor)")
                DispatchQueue.main.async {
                    self.view.activityStopAnimating()
                    self.listOfDoctor? = doctor
                    self.doctorsArray.append(contentsOf : doctor.data ?? [DoctorsData]())
                    self.doctorListTableView.reloadData()
                }
                break
                
            case .failure(let error):
                DispatchQueue.main.async {
                    self.view.activityStopAnimating()
                    print("Error Login \(error.localizedDescription)")
                    self.presentAlert(withTitle: "Error", message: ConstantStrings.noDoctorFound)
                }
                break
            }
            
        }
        
    }
    func sideMenus() {
        if revealViewController() != nil {
            revealViewController().rearViewRevealWidth = 290
            leftMenuButton.target = revealViewController()
            leftMenuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            //view.layer.cornerRadius = 23
        }
    }

}
extension DoctorsListVc : UITableViewDelegate ,UITableViewDataSource {
        
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
        return doctorsArray.count
        } else if section == 1 && fetchingMore {
            return 1
        }
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DoctorsListCell", for: indexPath) as! DoctorsListCell
        cell.configureCell(with: doctorsArray[indexPath.row])
        cell.selectionStyle = .none
        
        cell.scheduleWithDoctorOutlet.addTarget(self, action: #selector(scheduleWithDoctorVisitAction), for: UIControl.Event.touchUpInside)
        cell.scheduleWithDoctorOutlet.tag = indexPath.row
        return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "loadingCell", for: indexPath) as! LoadingCell
            cell.spinner.startAnimating()
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return tableView.frame.size.height/3
        }else if indexPath.section == 1 {
            return 70
        }
        return 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         index = doctorsArray[indexPath.row]
    }

    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {

      let offsetY = scrollView.contentOffset.y
      let contentHeight = scrollView.contentSize.height

      if offsetY > contentHeight - scrollView.frame.height * 1.5 {
          DispatchQueue.main.async {
              self.getNextPage()
          }
       }
    }
}

