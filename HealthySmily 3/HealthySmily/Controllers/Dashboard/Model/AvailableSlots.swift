//
//  AvailableSlots.swift
//  HealthySmily
//
//  Created by Zaid's MacBook on 31/01/2021.
//  Copyright © 2021 Zablesoft. All rights reserved.
//

import Foundation




import UIKit
import SwiftyJSON


struct Schedule {
    var timeKey: String = ""
    var startTime:String = ""
    var endTime:String = ""
    var available:Bool = false
}
struct NewSchedule {
    var timeKey: String = ""
    var startTime:String = ""
    var endTime:String = ""
    var available:Bool = false
}

class Availabity: NSObject {
    var dateKey: String = ""
    var slotsList: [Schedule] = []
    var isSorted:Bool = false

    override init() {
    }
    
    init(dateKey: String, slotsList: [Schedule]) {
        self.dateKey = dateKey
        self.slotsList = slotsList
    }

    static func fromJSON(_ jsonData:Data) -> [Availabity] {
        var json = JSON(jsonData)
        json = json["data"]

        var mainList:[Availabity] = []
        for (key, subJson) in json {
            let date = key
            var slotsList:[Schedule] = []
            for (key, subJson2) in subJson {
                let time = key
                let available = subJson2["available"].boolValue
                let startTime = subJson2["start_time"].stringValue
                let endTime = subJson2["end_time"].stringValue
                slotsList.append(Schedule(timeKey: time, startTime: startTime, endTime: endTime, available: available))
            }
            let obj = Availabity(dateKey: date, slotsList: slotsList)
            mainList.append(obj)
        }
        return mainList
    }
}
//extension Schedule: Comparable {
//    static func < (lhs: Schedule, rhs: Schedule) -> Bool {
//        if lhs.startTime != rhs.endTime {
//            return lhs.startTime < rhs.startTime
//        } else {
//            return lhs.endTime < rhs.endTime
//        }
//    }
//}
