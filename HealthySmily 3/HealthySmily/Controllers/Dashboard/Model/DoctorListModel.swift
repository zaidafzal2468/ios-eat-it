//
//  DoctorListModel.swift
//  HealthySmily
//
//  Created by Zaid's MacBook on 31/01/2021.
//  Copyright © 2021 Zablesoft. All rights reserved.
//

import Foundation



struct DoctorListModel : Codable {
    let links : Links?
    let data : [DoctorsData]?
    let paginator : Paginator?

    enum CodingKeys: String, CodingKey {

        case links = "links"
        case data = "data"
        case paginator = "paginator"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        links = try values.decodeIfPresent(Links.self, forKey: .links)
        data = try values.decodeIfPresent([DoctorsData].self, forKey: .data)
        paginator = try values.decodeIfPresent(Paginator.self, forKey: .paginator)
    }

}
struct NewDoctorsData : Codable {
    let id : Int?
    let name : String?
    let photo : String?
}
struct New2DoctorsData : Codable {
    let id : Int?
    let name : String?
    let photo : String?
}
struct New3DoctorsData : Codable {
    let id : Int?
    let name : String?
    let photo : String?
}
struct DoctorsData : Codable {
    let id : Int?
    let name : String?
    let photo : String?
    let qualification : String?
    let speciality : String?
    let gender : String?
    let fees : Int?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
        case photo = "photo"
        case qualification = "qualification"
        case speciality = "speciality"
        case gender = "gender"
        case fees = "fee"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        photo = try values.decodeIfPresent(String.self, forKey: .photo)
        qualification = try values.decodeIfPresent(String.self, forKey: .qualification)
        speciality = try values.decodeIfPresent(String.self, forKey: .speciality)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        fees = try values.decodeIfPresent(Int.self, forKey: .fees)

    }

}

struct Links : Codable {
    let selff : String?
    let first_page : String?
    let previous_page : String?
    let current_page : String?
    let next_page : String?
    let last_page : String?

    enum CodingKeys: String, CodingKey {

        case selff = "self"
        case first_page = "first_page"
        case previous_page = "previous_page"
        case current_page = "current_page"
        case next_page = "next_page"
        case last_page = "last_page"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        selff = try values.decodeIfPresent(String.self, forKey: .selff)
        first_page = try values.decodeIfPresent(String.self, forKey: .first_page)
        previous_page = try values.decodeIfPresent(String.self, forKey: .previous_page)
        current_page = try values.decodeIfPresent(String.self, forKey: .current_page)
        next_page = try values.decodeIfPresent(String.self, forKey: .next_page)
        last_page = try values.decodeIfPresent(String.self, forKey: .last_page)
    }

}
struct Paginator : Codable {
    let current_page : Int?
    let per_page : Int?
    let count : Int?
    let has_more_pages : Bool?
    let last_page : Int?
    let total : Int?

    enum CodingKeys: String, CodingKey {

        case current_page = "current_page"
        case per_page = "per_page"
        case count = "count"
        case has_more_pages = "has_more_pages"
        case last_page = "last_page"
        case total = "total"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        current_page = try values.decodeIfPresent(Int.self, forKey: .current_page)
        per_page = try values.decodeIfPresent(Int.self, forKey: .per_page)
        count = try values.decodeIfPresent(Int.self, forKey: .count)
        has_more_pages = try values.decodeIfPresent(Bool.self, forKey: .has_more_pages)
        last_page = try values.decodeIfPresent(Int.self, forKey: .last_page)
        total = try values.decodeIfPresent(Int.self, forKey: .total)
    }

}
