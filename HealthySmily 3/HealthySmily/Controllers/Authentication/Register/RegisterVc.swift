//
//  RegisterVc.swift
//  HealthySmily
//
//  Created by Zaid's MacBook on 30/01/2021.
//  Copyright © 2021 Zablesoft. All rights reserved.
//

import UIKit
import DropDown

class RegisterVc: UIViewController , UITextViewDelegate{
    
    @IBOutlet weak var userNameTxtField: UITextField!
    @IBOutlet weak var genderTxtField: UITextField!
    @IBOutlet weak var dobTxtField: UITextField!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var cnicTxtField: UITextField!
    @IBOutlet weak var diseaseNameTxtField: UITextField!
    @IBOutlet weak var diseaseDescTxtField: UITextView!
    @IBOutlet weak var createAccountOutlet: UIButton!
    @IBOutlet weak var report1Img: UIImageView!
    @IBOutlet weak var report2Img: UIImageView!

    
    
    var datePicker = UIDatePicker()
    let toolbar = UIToolbar()


    var dob = ""
    let dropDownGender = DropDown()
    var report1Image:String?
    var report2Image:String?
    var uploadreport1 = false
    var uploadreport2 = false


    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        showDatePicker()
        addGenderInDropDown()
        
        // Add tap gesture on image View
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(uploadReport1Tapped(tapGestureRecognizer:)))
        report1Img.isUserInteractionEnabled = true
        report1Img.addGestureRecognizer(tapGestureRecognizer1)
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(uploadReport2Tapped(tapGestureRecognizer:)))
        report2Img.isUserInteractionEnabled = true
        report2Img.addGestureRecognizer(tapGestureRecognizer2)

        // Do any additional setup after loading the view.
    }
    @IBAction func signUpAction (_ sender: Any){
        
        guard let firstname = userNameTxtField.text ,!firstname.isEmpty else  {
            self.presentAlert(withTitle: "", message: "Username is required.")
            return
        }
        guard let id = cnicTxtField.text ,!id.isEmpty else  {
            self.presentAlert(withTitle: "", message: "Cnic is required.")
            return
        }
        guard let email = emailTxtField.text ,!email.isEmpty else  {
            self.presentAlert(withTitle: "", message: "Email is required")
            return
        }
        guard  email.isValidEmail else {
            self.presentAlert(withTitle: "", message: "Invalid email format")
            return
        }
        guard let disease = diseaseNameTxtField.text ,!disease.isEmpty else  {
            self.presentAlert(withTitle: "", message: "Disease is required")
            return
        }
        guard let password = passwordTxtField.text ,!password.isEmpty else  {
            self.presentAlert(withTitle: "", message: "Password is required")
            return
        }
        let gender = genderTxtField.text?.lowercased() ?? ""
        
        let params = ["email": email,
                      "password": password,
                      "name": firstname,
                      "cnic": id,
                      "disease": diseaseNameTxtField.text ?? "",
                      "disease_desc": diseaseDescTxtField.text ?? "",
                       "dob" : dob,
                       "gender": gender,
                      "report_image": "data:image/png;base64\(report1Image ?? "")",
                      "infection_image": "data:image/png;base64\(report2Image ?? "")"]
                   
                      as [String : Any]
        
        
        
        if Utils.isConnectedToNetwork() == true {
            self.view.activityStartAnimating()
            AuthenticationService().registerPatient(params: params) { (response , errMsg)  in
                switch response {
                case .success(let patient):
                    print("Response : \(patient)")
                    DispatchQueue.main.async {
                        self.view.activityStopAnimating()
                        if errMsg?.count == 0 {
                            self.navigationController?.popViewController(animated: true)
                            print("Successfully register user")

                        }else {
                            self.presentAlert(withTitle: "Error", message: errMsg?[0] ?? "")
                        }

                    }
                    break

                case .failure(let error):
                    DispatchQueue.main.async {
                        self.view.activityStopAnimating()
                        print("Error Register \(error.localizedDescription)")
                        self.presentAlert(withTitle: "Error", message: ConstantStrings.errorInRegistering)
                    }
                }
            }
            
        } else {

            self.presentAlert(withTitle: ConstantStrings.networkErrorTitle, message: ConstantStrings.networkErrorMessage)
        }
        
   
    }
    @IBAction func backBtnPressed(_sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:- Image Tap Gestures
    @objc func uploadReport1Tapped(tapGestureRecognizer: UITapGestureRecognizer){
        //let tappedImage = tapGestureRecognizer.view as! UIImageView
        print("Profile Image tapped")
        self.showImagePicker()
        self.uploadreport1 = true
        self.uploadreport2 = false


    }
    @objc func uploadReport2Tapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        //let tappedImage = tapGestureRecognizer.view as! UIImageView
        print("Profile Image tapped")
        self.showImagePicker()
        self.uploadreport1 = false
        self.uploadreport2 = true

    }
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        //ToolBar
        toolbar.sizeToFit()
        datePicker = UIDatePicker.init(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 100))
        


        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        // add toolbar to textField
        dobTxtField.inputAccessoryView = toolbar
        // add datepicker to textField
        dobTxtField.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        //For date formate
        let formatter = DateFormatter()
        let date1 = "\(datePicker.date)"
        let date = datePicker.date

        let currentAge = date1.split{$0 == " "}.map(String.init)
        dob = currentAge[0]
        print("New Date is : \(dob)")
        
        formatter.dateFormat = "yyyy/MM/dd"
        let gregorian = Calendar(identifier: .gregorian)
        let ageComponents = gregorian.dateComponents([.year], from: date, to: Date())
        let age = ageComponents.year!
        dobTxtField.text = "\(age)"
        //dismiss date picker dialog
        self.view.endEditing(true)
    }
    
   @objc func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }
    //MARK:- Drop down Gender
    func addGenderInDropDown() {
        //let dropDown = DropDown()
        dropDownGender.anchorView = genderTxtField
        let dataSetArray:[String] = ["Male","Female"]
        
        dropDownGender.dataSource = dataSetArray
        dropDownGender.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.genderTxtField.isSelected = true
            self.genderTxtField.text = dataSetArray[index]
            self.genderTxtField.resignFirstResponder()
            
        }
    }
    func configureUI() {
        //diseaseDescTxtField.delegate = self
        diseaseDescTxtField.text = "Enter disease description"
        diseaseDescTxtField.textColor = UIColor.lightGray
        
        cnicTxtField.delegate = self
        userNameTxtField.delegate = self
        genderTxtField.delegate = self
        dobTxtField.delegate = self
        passwordTxtField.delegate = self
        emailTxtField.delegate = self
        diseaseNameTxtField.delegate = self
        
        userNameTxtField.backgroundColor = .white
        cnicTxtField.backgroundColor = .white
        dobTxtField.backgroundColor = .white
        passwordTxtField.backgroundColor = .white
        genderTxtField.backgroundColor = .white
        emailTxtField.backgroundColor = .white
        diseaseNameTxtField.backgroundColor = .white
        
        report1Img.dropShadowToAllSides()
        report2Img.dropShadowToAllSides()
        createAccountOutlet.layer.cornerRadius = 20

    }
    
   //MARK:- IMAGE PICKER DELEGATE METHOD
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let userSelectedImage = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)
        if uploadreport1 == true {
            report1Img.image = userSelectedImage
            let imgData = (userSelectedImage!).jpegData(compressionQuality: 0.5)!
            self.report1Image = imgData.base64EncodedString(options: .lineLength64Characters)
        }else if uploadreport2 == true {
            let imgData = (userSelectedImage!).jpegData(compressionQuality: 0.5)!
            self.report1Image = imgData.base64EncodedString(options: .lineLength64Characters)
            report2Img.image = userSelectedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.hexStringToUIColor(hex: ConstantStrings.AppColor)
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Enter disease description"
            textView.textColor = UIColor.lightGray
        }
    }
}
//MARK:- Text field delegate method

extension RegisterVc : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == genderTxtField {
            dropDownGender.show()
            textField.resignFirstResponder()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         if textField ==  cnicTxtField {
                     let allowedCharacters = "1234567890"
                     let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
                     let typedCharacterSet = CharacterSet(charactersIn: string)
                     let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
                     let Range = range.length + range.location > (cnicTxtField.text?.count)!

             if Range == false && alphabet == false {
                 return false
             }


             let NewLength = (cnicTxtField.text?.count)! + string.count - range.length
             return NewLength <= 15

         } else {
               return true
           }

       }
}
