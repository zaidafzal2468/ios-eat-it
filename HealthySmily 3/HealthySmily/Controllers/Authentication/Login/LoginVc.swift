//
//  LoginVc.swift
//  HealthySmily
//
//  Created by Zaid's MacBook on 30/01/2021.
//  Copyright © 2021 Zablesoft. All rights reserved.
//

import UIKit

class LoginVc: UIViewController {
    
    
    @IBOutlet weak var emailUIView: UIView!
    @IBOutlet weak var passwordUIView: UIView!
    @IBOutlet weak var emailTxtFieldOutlet: UITextField!
    @IBOutlet weak var passwordTxtFieldOutlet: UITextField!
    @IBOutlet weak var signInButtonOutlet: UIButton!
    @IBOutlet weak var dontHaveAccountOutlet: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        passwordTxtFieldOutlet.delegate = self

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let token = Session.instance.getLoginToken
        if token == nil {
            print("User is NOT logged in")
        }else {
            print("User is logged in")
            self.doLogin(token: token ?? "")
        }
        
    }
    @IBAction func loginBtnPressed(_sender :Any){
        guard let email = emailTxtFieldOutlet.text ,!email.isEmpty else  {
            self.presentAlert(withTitle: "", message: "Email field is required")
            return
        }
        guard  email.isValidEmail else {
            self.presentAlert(withTitle: "", message: "Invalid email format")
            return
        }
        
        guard let password = passwordTxtFieldOutlet.text ,!password.isEmpty else  {
            self.presentAlert(withTitle: "", message: "Password field is required")
            return
        }
        let params = ["email":email,
                      "password": password] as [String : Any]
        
        
        if Utils.isConnectedToNetwork() == true {
            self.view.activityStartAnimating()
            AuthenticationService().getLoginToken(params: params) { (response) in
                switch response {
                case .success(let userData):
                    Session.instance.saveBearerToken(token: userData.data?.api_token)
                    print("Success Get Token")
                    self.doLogin(token: userData.data?.api_token ?? "")
                    
                    break
                case .failure(let error):
                    
                    DispatchQueue.main.async {
                        self.view.activityStopAnimating()
                        print("Error Getting token \(error.localizedDescription)")
                        self.presentAlert(withTitle: "Error", message: ConstantStrings.inValidOrEmailPhoneNo)
                    }
                    break
                }
            }
        }else {
            self.presentAlert(withTitle: ConstantStrings.networkErrorTitle, message: ConstantStrings.networkErrorMessage)
        }
    }

    
    @IBAction func dontHaveAnAccount(_sender :Any){
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "RegisterVc") as! RegisterVc
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func doLogin(token: String) {
        AuthenticationService().loginPatient(token: token) { (response) in
            switch response {
            case .success(let user):
                print("Success Loggin")
                DispatchQueue.main.async {
                    LoginUserManager.sharedInstance.userData = user.data
                    self.navigateToDashboard()
                    self.view.activityStopAnimating()
                }
                break
            case .failure(let error):
                print("Failure Loggin")
                DispatchQueue.main.async {
                    self.view.activityStopAnimating()
                    print("Error Login \(error.localizedDescription)")
                    self.presentAlert(withTitle: "Error", message: ConstantStrings.inValidOrEmailPhoneNo)
                }
                break
            }
        }
    }

    func configureUI() {
       
        dontHaveAnAccount()
    }
    func navigateToDashboard(){
        
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    
    }
    
    func dontHaveAnAccount() {
        let attributedText = NSMutableAttributedString(string: "Don't have account?    ", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14) ,NSAttributedString.Key.foregroundColor: UIColor.gray])
        
        attributedText.append(NSAttributedString(string: "Create Account", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15), NSAttributedString.Key.foregroundColor: UIColor(red: 1/255, green: 146/255, blue: 210/255, alpha: 1).cgColor]))
        dontHaveAccountOutlet.setAttributedTitle(attributedText, for: .normal)
    }
}
//MARK:- TextField Delegate Method

extension LoginVc : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //Prevent "0" characters as the first characters. (i.e.: There should not be values like "003" "01" "000012" etc.)
        if emailTxtFieldOutlet.text?.count == 0 && string == "0" {
            return false
        }
        //Limit the character count to 10.
        if ((emailTxtFieldOutlet.text!) + string).count > 30 {
            return false
        }
        return true
        
    }
    
}
