//
//  LoginService.swift
//  HealthySmily
//
//  Created by Zaid's MacBook on 30/01/2021.
//  Copyright © 2021 Zablesoft. All rights reserved.
//

import Foundation
//
//  SignupManagerService.swift
//  VirtualClinic
//
//  Created by Zaid's MacBook on 01/07/2020.
//  Copyright © 2020 Whetstonez. All rights reserved.
//

import Foundation
import Alamofire

class AuthenticationService {

    //MARK:- Register Patient
    
    func registerPatient (params : Parameters , complition :  @escaping ( (Result<LoginModel, Error > , _ errorMsg : [String]?)-> ())  ) {
        let headers: HTTPHeaders = [
            "Accept":"application/json",
           // "Content-Type":"application/json"
        ]
        NetworkManager.sharedInstance.sendPostRequest(url: baseUrl + "api/user/register", parameters: params, headers: headers) { (response) in

                do{
                    let json = try JSONSerialization.jsonObject(with: response.data!) as! Dictionary<String, AnyObject>
                    print("JSON ::\(json)")
                    let decoder = JSONDecoder()
                    let patient = try decoder.decode(LoginModel.self, from: response.data!)
                    complition(.success(patient), patient.errors)
                    print("Success Decode register \(patient)")
                } catch let jsonError{
                    print("Failure Decode Register \(jsonError)")
                    complition(.failure(jsonError), nil)
                }
            }
        
    }
    //MARK:-Get Login Token Patient
    func getLoginToken (params : Parameters , complition :  @escaping ( (Result<LoginModel, Error>)-> ())  ) {
        let headers: HTTPHeaders = [
            "Accept":"application/json",
           // "Content-Type":"application/json"
        ]
        NetworkManager.sharedInstance.sendPostRequest(url: baseUrl + "api/user/login", parameters: params, headers: headers) { (response) in
            
            if response.error != nil {
                complition(.failure(response.error!))
            }else {
                do{
                    let json = try JSONSerialization.jsonObject(with: response.data!) as! Dictionary<String, AnyObject>
                    print("JSON ::\(json)")
                    let decoder = JSONDecoder()
                    let patient = try decoder.decode(LoginModel.self, from: response.data!)
                    complition(.success(patient))
                    print("Service Response \(patient)")
                } catch let jsonError{
                    print("Failure Response \(jsonError)")
                    complition(.failure(jsonError))
                }
            }
        }
    }
    //MARK:- Login Patient
    func loginPatient (token: String ,complition :  @escaping ( (Result<LoginModel, Error>)-> ())  ) {
        let headers: HTTPHeaders = [
            .accept("application/json"),
            .authorization("Bearer \(token)")
        ]
        
        NetworkManager.sharedInstance.sendGetRequest(url: baseUrl + "api/user/me", headers: headers, params: nil) { (response) in
            
            if response.error != nil {
                complition(.failure(response.error!))
            }else {
                do{
                    let json = try JSONSerialization.jsonObject(with: response.data!) as! Dictionary<String, AnyObject>
                    print("JSON ::\(json)")
                    let decoder = JSONDecoder()
                    let patient = try decoder.decode(LoginModel.self, from: response.data!)
                    complition(.success(patient))
                    print("Service Response \(patient)")
                } catch let jsonError{
                    print("Failure Response \(jsonError)")
                    complition(.failure(jsonError))
                }
            }
        }
    }
}


