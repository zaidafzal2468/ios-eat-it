//
//  LoginModel.swift
//  HealthySmily
//
//  Created by Zaid's MacBook on 30/01/2021.
//  Copyright © 2021 Zablesoft. All rights reserved.
//

import Foundation

//Mark:-Login Model

struct LoginModel : Decodable{
    let status: Bool?
    let message: String?
    var data: UserData?
    let errors : [String]?
    
    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
        case errors = "errors"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        //data = ( try values.decodeIfPresent(UserData.self, forKey: .data))
        errors = try values.decodeIfPresent([String].self, forKey: .errors)
        
        if let d = try? values.decodeIfPresent(UserData.self, forKey: .data) {
                data = d
            }
        
    }


}

struct UserData : Codable {
    
    
    
    let email : String?
    let name : String?
    let cnic : String?
    let api_token : String?
    let disease : String?
    let disease_desc : String?
    let report_image : String?
    let infection_image : String?
    let id : Int?

    enum CodingKeys: String, CodingKey {

        case email = "email"
        case name = "name"
        case cnic = "cnic"
        case disease = "disease"
        case disease_desc = "disease_desc"
        case report_image = "report_image"
        case infection_image = "infection_image"
        case id = "id"
        case api_token = "api_token"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        cnic = try values.decodeIfPresent(String.self, forKey: .cnic)
        disease = try values.decodeIfPresent(String.self, forKey: .disease)
        disease_desc = try values.decodeIfPresent(String.self, forKey: .disease_desc)
        report_image = try values.decodeIfPresent(String.self, forKey: .report_image)
        infection_image = try values.decodeIfPresent(String.self, forKey: .infection_image)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        api_token = try values.decodeIfPresent(String.self, forKey: .api_token)
    }
}

