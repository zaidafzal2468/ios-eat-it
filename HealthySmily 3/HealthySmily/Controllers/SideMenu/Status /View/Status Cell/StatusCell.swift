//
//  StatusCell.swift
//  HealthySmily
//
//  Created by Zaid's MacBook on 04/02/2021.
//  Copyright © 2021 Zablesoft. All rights reserved.
//

import UIKit

class StatusCell: UITableViewCell {
    
    
    @IBOutlet weak var doctorNameLbl: UILabel!
    @IBOutlet weak var dateTimeLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var chatBtn: UIButton!




    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
