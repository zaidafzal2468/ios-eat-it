//
//  ChatCell.swift
//  HealthySmily
//
//  Created by Zaid's MacBook on 23/04/2021.
//  Copyright © 2021 Zablesoft. All rights reserved.
//

import UIKit

class ChatCell: UITableViewCell {
    
    @IBOutlet weak var senderBubbleView: UIView!
    @IBOutlet weak var messageBubbleView: UIView!
    @IBOutlet weak var messageStackView: UIStackView!
    @IBOutlet weak var senderNameLabel: UILabel!
    @IBOutlet weak var senderMessageTxt: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
