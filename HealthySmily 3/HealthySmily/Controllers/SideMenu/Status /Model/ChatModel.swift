//
//  ChatModel.swift
//  HealthySmily
//
//  Created by Zaid's MacBook on 23/04/2021.
//  Copyright © 2021 Zablesoft. All rights reserved.
//

import Foundation

//SEnt message model
struct ChatModel : Codable {
    let status : Bool?
    let message : String?
    let data : ChatData?
    let errors : [String]?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
        case errors = "errors"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(ChatData.self, forKey: .data)
        errors = try values.decodeIfPresent([String].self, forKey: .errors)
    }

}
struct ChatData : Codable {
    let appointment_id : String?
    let message : String?
    let by : String?
    let status : String?
    let id : Int?

    enum CodingKeys: String, CodingKey {

        case appointment_id = "appointment_id"
        case message = "message"
        case by = "by"
        case status = "status"
        case id = "id"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        appointment_id = try values.decodeIfPresent(String.self, forKey: .appointment_id)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        by = try values.decodeIfPresent(String.self, forKey: .by)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
    }

}


//Get message model
struct GetMessageModel : Codable {
    let links : Links?
    let data : [GetMessageData]?
    let paginator : Paginator?

    enum CodingKeys: String, CodingKey {

        case links = "links"
        case data = "data"
        case paginator = "paginator"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        links = try values.decodeIfPresent(Links.self, forKey: .links)
        data = try values.decodeIfPresent([GetMessageData].self, forKey: .data)
        paginator = try values.decodeIfPresent(Paginator.self, forKey: .paginator)
    }

}

struct GetMessageData : Codable {
    let id : Int?
    let appointment_id : Int?
    let message : String?
    let by : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case appointment_id = "appointment_id"
        case message = "message"
        case by = "by"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        appointment_id = try values.decodeIfPresent(Int.self, forKey: .appointment_id)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        by = try values.decodeIfPresent(String.self, forKey: .by)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}
