//
//  StatusModel.swift
//  HealthySmily
//
//  Created by Zaid's MacBook on 04/02/2021.
//  Copyright © 2021 Zablesoft. All rights reserved.
//

import Foundation
struct StatusModel : Codable {
    let links : Links?
    let data : [StatusData]?
    let paginator : Paginator?

    enum CodingKeys: String, CodingKey {

        case links = "links"
        case data = "data"
        case paginator = "paginator"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        links = try values.decodeIfPresent(Links.self, forKey: .links)
        data = try values.decodeIfPresent([StatusData].self, forKey: .data)
        paginator = try values.decodeIfPresent(Paginator.self, forKey: .paginator)
    }

}
struct StatusData : Codable {
    let id : Int?
    let date_time : String?
    let fee : Int?
    let status : String?
    let created_at : String?
    let user : StatusUser?
    let doctor : StatusDoctor?

}

struct StatusDoctor : Codable {
    let id : Int?
    let name : String?
    let photo : String?
    let fee : Int?
    let qualification : String?
    let speciality : String?
    let gender : String?
}
struct StatusUser : Codable {
    let id : Int?
    let name : String?
    let email : String?
    let cnic : String?
    let dob : String?
    let api_token : String?
    let disease : String?
    let gender : String?
    let disease_desc : String?
    let report_image : String?
    let infection_image : String?
    
}
