//
//  StatusVc.swift
//  HealthySmily
//
//  Created by Zaid's MacBook on 04/02/2021.
//  Copyright © 2021 Zablesoft. All rights reserved.
//

import UIKit

class StatusVc: UIViewController {
    
    
    @IBOutlet weak var sideMenu: UIBarButtonItem!

    @IBOutlet weak var statusTblView: UITableView!
    
    
    var statusArray = [StatusData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuControlller()
        getAppointmentsStatus()
        // Do any additional setup after loading the view.
    }
    
    func sideMenuControlller(){
        
        sideMenu.target = self.revealViewController()
        sideMenu.action = #selector(SWRevealViewController.revealToggle(_:))
        self.revealViewController().rearViewRevealWidth = 290
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    
    func getAppointmentsStatus(){
        self.view.activityStartAnimating()
        SideMenuService.getStatusOfAppointments() { (response) in
          //  self.fetchingMore = false
            switch response {
            case .success(let doctor):
                self.view.activityStopAnimating()

                print("Response : \(doctor)")
                self.statusArray = doctor.data ?? []
                self.statusTblView.reloadData()
                break
                
            case .failure(let error):
                DispatchQueue.main.async {
                    self.view.activityStopAnimating()
                    print("Error Getting doctor \(error.localizedDescription)")
                    self.presentAlert(withTitle: "Error", message: ConstantStrings.noDoctorFound)
                }
                break
            }
            
        }

    }


}
extension StatusVc : UITableViewDelegate ,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statusArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StatusCell", for: indexPath) as! StatusCell
        cell.doctorNameLbl.text = statusArray[indexPath.row].doctor?.name
        cell.dateTimeLbl.text = statusArray[indexPath.row].date_time
        cell.statusLbl.text = statusArray[indexPath.row].status
        
        let status = statusArray[indexPath.row].status
        if status == "accepted" {
            cell.chatBtn.isHidden = false
        }else {
            cell.chatBtn.isHidden = true
        }


        
//        cell.itemImageView.image = UIImage(named :menuIds[indexPath.row])
          cell.statusView.addShadowToBottomLayer()
//        cell.selectionStyle = .none

        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let status = statusArray[indexPath.row].status
        if status == "accepted" {
            let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
             let vc = storyboard.instantiateViewController(withIdentifier: "ChatWithDoctorVc") as! ChatWithDoctorVc
            vc.appointmentId = "\(statusArray[indexPath.row].id ?? 0)"
             navigationController?.pushViewController(vc,
                                                      animated: true)
            
        }
    }

    
}

