//
//  ChatWithDoctorVc.swift
//  HealthySmily
//
//  Created by Zaid's MacBook on 23/04/2021.
//  Copyright © 2021 Zablesoft. All rights reserved.

import UIKit
import Alamofire

class ChatWithDoctorVc: UIViewController,UITextViewDelegate {

    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var textFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var textViewField: UITextView!
    @IBOutlet weak var doctorSpecialityLabel: UILabel!
    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var doctorImageView: UIImageView!
    @IBOutlet weak var chatingTableView: UITableView!
    @IBOutlet weak var sideMenu: UIBarButtonItem!

    
    var appointmentId = ""

    var messagesArray = [GetMessageData]()
    
    //left bar button Item
    let leftMenuButton = UIBarButtonItem(image: UIImage(named: "sideMenuIcon")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(leftMenuAction))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        sideMenus()
        registerNib()
        let timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.updateChat), userInfo: nil, repeats: true)
        timer.fire()
        sideMenuControlller()

    }
    @objc func updateChat() {
        getChatMessages()
    }
    
    func sideMenuControlller(){
        
        sideMenu.target = self.revealViewController()
        sideMenu.action = #selector(SWRevealViewController.revealToggle(_:))
        self.revealViewController().rearViewRevealWidth = 290
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    func textViewDidChange(_ textView: UITextView) {
        let sizeToFitIn = CGSize(width: self.textViewField.bounds.size.width, height: 38)
        let newSize = self.textViewField.sizeThatFits(sizeToFitIn)
        self.textFieldHeight.constant = newSize.height
    }
    func sideMenus() {
        if revealViewController() != nil {
            revealViewController().rearViewRevealWidth = 290
            leftMenuButton.target = revealViewController()
            leftMenuButton.action = #selector(SWRevealViewController.revealToggle(_:))
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    @objc func leftMenuAction(){
         print("left clicked")
    }

    @IBAction func sendMessageAction(_ sender: Any) {
        sentMessageToDoctor()
    }
    @IBAction func goBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getChatMessages(){
      //  self.view.activityStartAnimating()
        SideMenuService.getChatMessages(appointmentId: appointmentId) { (response) in
            switch response {
            case .success(let user):
                print("Success get message")
                DispatchQueue.main.async {
                    self.messagesArray = user.data ?? []
                    self.chatingTableView.reloadData()
                   // self.view.activityStopAnimating()
                }
                break
            case .failure(let error):
                print("Failure Loggin")
                DispatchQueue.main.async {
                 //   self.view.activityStopAnimating()
                    print("Error Login \(error.localizedDescription)")
                    self.presentAlert(withTitle: "Error", message: ConstantStrings.errorInLoadingMsg)
                }
                break
            }
        }
    }
    
    func sentMessageToDoctor(){
        let params = ["appointment_id": appointmentId,
                      "message": textViewField.text ?? ""]
                   
                      as [String : Any]
      //  self.view.activityStartAnimating()

        SideMenuService.sendMessageService(params: params) { (response , error) in
            switch response {
            case .success(let patient):
                print("Sussfully send message : \(patient)")
                DispatchQueue.main.async {
                  //  self.view.activityStopAnimating()
         

                }
                break

            case .failure(let error):
                DispatchQueue.main.async {
               //     self.view.activityStopAnimating()
                    print("Error Register \(error.localizedDescription)")
                    self.presentAlert(withTitle: "Error", message: ConstantStrings.errorInSendMsg)
                }
            }
        }
    }
    func configureUI() {
        containerView.addLayout()
        doctorImageView.makeRounded()
        doctorImageView.layer.borderColor = UIColor.white.cgColor
        textViewField.textContainerInset = UIEdgeInsets(top: 4, left: 4, bottom: 0, right: 35)
        containerView.addLayout()
        messageView.addLayout()
        messageView.layer.cornerRadius = 10
        //To hide the border of nav bar
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
        self.navigationItem.leftBarButtonItem  = leftMenuButton

    }
    func registerNib() {
        chatingTableView.register(UINib(nibName: "ChatCell", bundle: nil), forCellReuseIdentifier: "ChatCell")
        chatingTableView.estimatedRowHeight = 50
        chatingTableView.rowHeight = UITableView.automaticDimension
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textViewField.text = ""
        textViewField.textColor = .black
        print("print1")
    }
}
//MARK:- TableView Delegate and DataSources Methods

extension ChatWithDoctorVc : UITableViewDelegate ,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messagesArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell", for: indexPath) as! ChatCell
        cell.selectionStyle = .none
        cell.senderMessageTxt.text = messagesArray[indexPath.row].message

        if messagesArray[indexPath.row].by == "patient" {
        cell.messageStackView.alignment = .trailing
        cell.messageBubbleView.backgroundColor = .white
        cell.senderBubbleView.backgroundColor = .white
        cell.messageBubbleView.addShadowToBottomLayer()
        cell.messageBubbleView.layer.shadowColor = UIColor.darkGray.cgColor
        cell.senderNameLabel.text = "Me"

        }else {
            cell.messageStackView.alignment = .leading
            cell.senderNameLabel.text = "Doctor"
        }

        return cell
    }

}
