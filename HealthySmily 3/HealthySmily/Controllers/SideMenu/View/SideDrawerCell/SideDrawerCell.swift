//
//  SideDrawerCell.swift
//  VirtualClinic
//
//  Created by Zaid's MacBook on 22/06/2020.
//  Copyright © 2020 Whetstonez. All rights reserved.
//

import UIKit

class SideDrawerCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var itemTxtLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
