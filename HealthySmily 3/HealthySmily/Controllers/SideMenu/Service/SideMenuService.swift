//
//  SideMenuService.swift
//  HealthySmily
//
//  Created by Zaid's MacBook on 04/02/2021.
//  Copyright © 2021 Zablesoft. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class SideMenuService {
    
    
    static func getStatusOfAppointments(complition :  @escaping ( (Result<StatusModel, Error>)-> ()) ) {
        
        let token = Session.instance.getLoginToken!
        let headers: HTTPHeaders = [
            .accept("application/json"),
            .authorization("Bearer \(token)")
        ]
        
        NetworkManager.sharedInstance.sendGetRequest(url: baseUrl2 + "appointments", headers: headers, params: nil) { (response) in
    
            if response.error != nil {
                complition(.failure(response.error!))
            }else {
                do{
                    let json = JSON(response.data!)
                    print("JSON ::\(json)")
                    let decoder = JSONDecoder()
                    let patient = try decoder.decode(StatusModel.self, from: response.data!)
                    complition(.success(patient))
                    print("Service Response \(patient)")
                } catch let jsonError{
                    print("Failure Response \(jsonError)")
                    complition(.failure(jsonError))
                }
            }
        }
    }
    
    static func getChatMessages(appointmentId:String,complition :  @escaping ( (Result<GetMessageModel, Error>)-> ()) ) {
        
        let token = Session.instance.getLoginToken!
        let headers: HTTPHeaders = [
            .accept("application/json"),
            .authorization("Bearer \(token)")
        ]
        
        NetworkManager.sharedInstance.sendGetRequest(url: baseUrl2 + "appointments/message?appointment_id=\(appointmentId)", headers: headers, params: nil) { (response) in
    
            if response.error != nil {
                complition(.failure(response.error!))
            }else {
                do{
                    let json = JSON(response.data!)
                    print("JSON ::\(json)")
                    let decoder = JSONDecoder()
                    let patient = try decoder.decode(GetMessageModel.self, from: response.data!)
                    complition(.success(patient))
                    print("Service Response \(patient)")
                } catch let jsonError{
                    print("Failure Response \(jsonError)")
                    complition(.failure(jsonError))
                }
            }
        }
    }

    
    static func sendMessageService (params : Parameters , complition :  @escaping ( (Result<ChatModel, Error > , _ errorMsg : [String]?)-> ())  ) {
        let token = Session.instance.getLoginToken!

        let headers: HTTPHeaders = [
            .accept("application/json"),
            .authorization("Bearer \(token)")
        ]
        NetworkManager.sharedInstance.sendPostRequest(url: baseUrl2 + "appointments/message", parameters: params, headers: headers) { (response) in

                do{
                    let json = try JSONSerialization.jsonObject(with: response.data!) as! Dictionary<String, AnyObject>
                    print("JSON ::\(json)")
                    let decoder = JSONDecoder()
                    let patient = try decoder.decode(ChatModel.self, from: response.data!)
                    complition(.success(patient), patient.errors)
                    print("Success Decode register \(patient)")
                } catch let jsonError{
                    print("Failure Decode Register \(jsonError)")
                    complition(.failure(jsonError), nil)
                }
            }
        
    }
}
