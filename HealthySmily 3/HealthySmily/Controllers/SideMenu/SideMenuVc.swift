//
//  SideMenuVc.swift
//  HealthySmily
//
//  Created by Zaid's MacBook on 02/02/2021.
//  Copyright © 2021 Zablesoft. All rights reserved.
//

import UIKit

class SideMenuVc: UIViewController {


    @IBOutlet weak var sideMenuButtons: UIView!
    @IBOutlet weak var sideMenuButtons1: UIView!
    @IBOutlet weak var sideMenuButtons2: UIView!
    @IBOutlet weak var sideMenuButtons3: UIView!

    @IBOutlet weak var userView: UIView!
    @IBOutlet weak var goToProfileOutlet: UIButton!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var sideDrawerView: UIView!
    @IBOutlet weak var sideDrawerTableView: UITableView!
    

    
    var menuOptions = ["Edit Profile", "Dashboard", "Status", "Chat" ,"Sign Out"]
    var menuIds = ["EditProfile","Dashboard","Status","Terms_Conditions","Logout"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
       // registerNib()


    }
    
    @IBAction func viewProfileAction(_ sender: Any) {
        self.performSegue(withIdentifier: "EditProfile", sender: self)
    }
    @IBAction func dahboardBtnPressed(_ sender: Any) {
        
        
        self.performSegue(withIdentifier: "Dashboard", sender: self)

        
    }
    
    @IBAction func btnLogOutTapped(_ sender: Any){
        Alert.showWithTwoActions(title: "Log Out", msg: "Are you sure want to log out", okBtnTitle: "Yes", okBtnAction: {
            UserDefaults.standard.removeObject(forKey: "token")
            self.navigationController?.popToRootViewController(animated: true)
        }, cancelBtnTitle: "No") {
            
        }
    }
    
    @IBAction func statusBtnPressed(_ sender: Any) {
        
        
        self.performSegue(withIdentifier: "StatusVc", sender: self)

        
    }
    @IBAction func searchButtonAction(_ sender: Any) {
        
    }
    func configureUI(){
        self.sideDrawerView.layer.roundCorners([.leftTop,.rightBottom], corner: 23)
        sideDrawerView.layer.borderWidth = 1
        sideDrawerView.layer.borderColor = UIColor(red: 1/255, green: 146/255, blue: 210/255, alpha: 1).cgColor
        sideDrawerView.layer.masksToBounds = false
        sideDrawerView.layer.shadowColor = UIColor(red: 1/255, green: 146/255, blue: 210/255, alpha: 1).cgColor
        sideDrawerView.layer.shadowOpacity = 0.5
        sideDrawerView.layer.shadowOffset = CGSize(width: 1.5, height: 1)
        sideDrawerView.layer.shadowRadius = 1
       // userImageView.makeRounded()
        userView.addShadowToBottomLayer()
        //To remove extra lines below tableview
      //  sideDrawerTableView.tableFooterView = UIView()
        let data = LoginUserManager.sharedInstance.userData
        userNameLabel.text = data?.name
        
        sideMenuButtons.addShadowToBottomLayer()
        sideMenuButtons1.addShadowToBottomLayer()
        sideMenuButtons2.addShadowToBottomLayer()
        sideMenuButtons3.addShadowToBottomLayer()


        
    }
    func registerNib() {
        sideDrawerTableView.register(UINib(nibName: "SideDrawerCell", bundle: nil), forCellReuseIdentifier: "SideDrawerCell")
    }
    

}/*
//MARK:- TableView Delegates and Datasources
extension SideMenuVc : UITableViewDelegate ,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideDrawerCell", for: indexPath) as! SideDrawerCell
        cell.itemTxtLabel.text = menuOptions[indexPath.row]
        cell.itemImageView.image = UIImage(named :menuIds[indexPath.row])
        cell.containerView.addShadowToBottomLayer()
        cell.selectionStyle = .none

        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        self.performSegue(withIdentifier: menuIds[indexPath.row], sender: self)
      /*  let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: menuIds[indexPath.row])
        navigationController?.pushViewController(vc,
                                                 animated: true)*/
    }
    
}
*/
