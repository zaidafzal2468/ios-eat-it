//
//  EditProfileVc.swift
//  HealthySmily
//
//  Created by Zaid's MacBook on 31/01/2021.
//  Copyright © 2021 Zablesoft. All rights reserved.
//

import UIKit

class EditProfileVc: UIViewController {
    
    
    @IBOutlet weak var nameTxtField: UITextField!
    @IBOutlet weak var cnicTxtField: UITextField!
    @IBOutlet weak var diseaseNameTxtField: UITextField!
    @IBOutlet weak var ageTxtField: UITextField!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var updateProfilePicOutlet: UIButton!
    @IBOutlet weak var report1ImageView: UIImageView!
    @IBOutlet weak var report2ImageView: UIImageView!
    @IBOutlet weak var sideMenu: UIBarButtonItem!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuControlller()
        // Do any additional setup after loading the view.
    }
    
    func sideMenuControlller(){
        
        sideMenu.target = self.revealViewController()
        sideMenu.action = #selector(SWRevealViewController.revealToggle(_:))
        self.revealViewController().rearViewRevealWidth = 290
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }


}
