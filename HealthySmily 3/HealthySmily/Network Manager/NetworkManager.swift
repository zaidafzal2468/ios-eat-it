//
//  NetworkManager.swift
//  HealthySmily
//
//  Created by Zaid's MacBook on 30/01/2021.
//  Copyright © 2021 Zablesoft. All rights reserved.
//

import Foundation
import Alamofire

class NetworkManager: NSObject {
    
    static let sharedInstance = NetworkManager()
    //MARK:- Get Method

    func sendGetRequest(url: String, headers: HTTPHeaders? ,params: Parameters?, completion: @escaping (AFDataResponse<String>) -> Void) -> Void {
        AF.request(url, method: .get ,headers: headers).validate().responseString { response in
            completion(response)
        }
    }
    //MARK:- Delete Method

    func sendDeleteRequest(url: String, headers: HTTPHeaders? ,params: Parameters?, completion: @escaping (AFDataResponse<String>) -> Void) -> Void {
        AF.request(url, method: .delete ,headers: headers).validate().responseString { response in
            completion(response)
        }
    }
    //MARK:- Put Method
    func sendPutRequest(url: String, headers: HTTPHeaders? ,params: Parameters?, completion: @escaping (AFDataResponse<String>) -> Void) -> Void {
        AF.request(url, method: .put,parameters: params,headers: headers).validate().responseString { response in
            completion(response)
        }
    }
    //MARK:- Post Method

    func sendPostRequest(url: String, parameters: Parameters, headers: HTTPHeaders, completion: @escaping (AFDataResponse<String>) -> Void) -> Void {
        
        AF.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .validate()
            .responseString { (response) in
                completion(response)
        }
    }
    func sendUploadImageRequest(url: String, parameters: Parameters, images: NSMutableDictionary, completion: @escaping (AFDataResponse<String>) -> Void) -> Void {
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data",
            "Accept": "application/json"
        ]
        AF.upload(multipartFormData: { multipartFormData in
            for img in images {
                let name = img.key as! String
                let imgData = (img.value as! UIImage).jpegData(compressionQuality: 0.3)!
                multipartFormData.append(imgData, withName: name,fileName: "\(Date().timeIntervalSinceNow)image.jpg", mimeType: "image/jpg")
            }
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: baseUrl + url, method: .post , headers: headers)
            .responseString { resp in
                switch resp.result{
                case .failure(let error):
                    print("Error Occur\(error.localizedDescription)")
                    completion(resp)
                    //self.activityStopAnimating()
                    print(error)
                case.success( _):
                    print("Successfully added user")
                    completion(resp)
                    // self.view.activityStopAnimating()
            }
        }
    }
}
