//
//  Session.swift
//  HealthySmily
//
//  Created by Zaid's MacBook on 31/01/2021.
//  Copyright © 2021 Zablesoft. All rights reserved.
//


import Foundation
class Session {
    static let instance = Session()
    private init() {
    }
    //MARK:- Save User login Status
    func saveUserLoginStatus(isLogin: Bool) {
        UserDefaults.standard.set(isLogin, forKey: "isLogin")
        UserDefaults.standard.synchronize()
    }
    func getUserLoginStatus() -> Bool {
        let status = UserDefaults.standard.value(forKey: "isLogin") as! Bool?
        return status ?? false
    }
    //MARK:- Save User Credentials
    func saveUserCredentials(userName :String) {
        UserDefaults.standard.set(userName, forKey: "userName")
    }
    var getUserName: String {
        get {
            return UserDefaults.standard.string(forKey: "userName")!

        }
    }
    func saveBearerToken(token: String?){
        UserDefaults.standard.set(token, forKey: "token")
    }
    var getLoginToken: String? {
        get {
            return UserDefaults.standard.string(forKey: "token")
        }
    }
    //MARK:- Save User Number
    func saveUserNumber(number: String){
        UserDefaults.standard.set(number, forKey: "phoneNo")
    }
    var getUserNumber: String {
        get {
            return UserDefaults.standard.string(forKey: "phoneNo")!

        }
    }
}
